import org.pmw.tinylog.Logger;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import static java.lang.Boolean.FALSE;
import static java.lang.Boolean.TRUE;


public class Main {

    public static void findVehicle(String vehicleType){

        int maximumSpeed = 0;
        Vehicle fastestVehicle = null;

        List<Vehicle> vehiclesList = new ArrayList<>();
        vehiclesList.add(new Vehicle("CAR", "BMW", 190));
        vehiclesList.add(new Vehicle("CAR", "MERCEDES", 195));
        vehiclesList.add(new Vehicle("SHIP", "TITANIC", 45));
        vehiclesList.add(new Vehicle("SHIP", "QUEEN MARY 2", 47));
        vehiclesList.add(new Vehicle("PLANE", "BOEING", 890));
        vehiclesList.add(new Vehicle("PLANE", "BOMBARDIER", 45));
        vehiclesList.add(new Vehicle("BICYCLE", "TREK", 34));
        vehiclesList.add(new Vehicle("BICYCLE", "GIANT", 40));
        vehiclesList.add(new Vehicle("CAR", "VW", 195));
        vehiclesList.add(new Vehicle("BICYCLE", "CROSS", 35));

        for (Vehicle vehicle : vehiclesList) {
            if (vehicle.getVehicleType().equals(vehicleType)){
                if (vehicle.maximumSpeed >= maximumSpeed) {
                    maximumSpeed = vehicle.maximumSpeed;
                    fastestVehicle = vehicle;
                }
            }
        }

        if (vehicleType.equals("ALL")){
            for (Vehicle vehicle : vehiclesList) {
                    if (vehicle.maximumSpeed >= maximumSpeed) {
                        maximumSpeed = vehicle.maximumSpeed;
                        fastestVehicle = vehicle;
                    }
            }
        }

        Logger.info("Vehicle " + fastestVehicle.getVehicleType() + " producer's " + fastestVehicle.getProducer() + " is fastest(maximum speed is " + fastestVehicle.getMaximumSpeed() + ")");
    }

    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        boolean runProgram = TRUE;
        do {
            Logger.info("Please select one of the available options: CAR, SHIP, PLANE, BICYCLE, ALL, EXIT");
            String selectedOption =scan.nextLine().toUpperCase();
            switch (selectedOption) {
                case "CAR":
                    findVehicle("CAR");
                    break;
                case "SHIP":
                    findVehicle("SHIP");
                    break;
                case "PLANE":
                    findVehicle("PLANE");
                    break;
                case "BICYCLE":
                    findVehicle("BICYCLE");
                    break;
                case "ALL":
                    findVehicle("ALL");
                    break;
                case "EXIT":
                    runProgram = FALSE;
                    System.exit(0);
                    break;
                default:
                    Logger.error("Introduce correct value");
            }
        }
        while(runProgram);
    }
}
