public class Vehicle {

    String vehicleType;
    String producer;
    int maximumSpeed;

    public Vehicle(String vehicleType, String producer, int maximumSpeed) {
        this.vehicleType = vehicleType;
        this.producer = producer;
        this.maximumSpeed = maximumSpeed;
    }

    public String getVehicleType() {
        return vehicleType;
    }

    public void setVehicleType(String vehicleType) {
        this.vehicleType = vehicleType;
    }

    public String getProducer() {
        return producer;
    }

    public void setProducer(String producer) {
        this.producer = producer;
    }

    public int getMaximumSpeed() {
        return maximumSpeed;
    }

    public void setMaximumSpeed(int maximumSpeed) {
        this.maximumSpeed = maximumSpeed;
    }
}
